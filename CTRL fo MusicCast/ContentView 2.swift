//
//  ContentView.swift
//  CTRL fo MusicCast
//
//  Created by M. Lierop on 04/08/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = 0
 
    var body: some View {
        TabView(selection: $selection){
            Text("System Power")
                .font(.title)
                .tabItem {
                    VStack {
                        Image("first")
                        Text("First")
                        Toggle()
                    }
                }
                .tag(0)
            Text("Select Source")
                .font(.title)
                .tabItem {
                    VStack {
                        Image("second")
                        Text("Second")
                    }
                }
                .tag(1)
        }
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
