//
//  ContentView.swift
//  CTRL fo MusicCast
//
//  Created by M. Lierop on 04/08/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = 0
    @State var showGreeting = true
    @ObjectBinding var mcm = MCMiddleMan()
    
    var body: some View {
        
        VStack{
            Text("Yamaha Stereo toren set")
            Toggle(isOn: self.$mcm.powerIsOn) {
                Text("System Power")
                
            }.padding()

            
        }
    }
}




#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
