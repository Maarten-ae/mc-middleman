//
//  MCMiddleMan.swift
//  CTRL fo MusicCast
//
//  Created by M. Lierop on 05/08/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//

import Foundation
import Combine
import UIKit
import SwiftUI

class MCMiddleMan: ObservableObject {
    
    var didChange = PassthroughSubject<Void, Never>()

    let TskPowerOn = URL(string: "http://192.168.1.149/YamahaExtendedControl/v1/main/setPower?power=toggle")!
    
    //@Published var powerIsOn: Bool = true
    
    @Published var powerIsOn = true {
        didSet {
            
            let Task = URLSession.shared.dataTask(with: TskPowerOn) { (data, response, error) in
                if error == nil {
                    print("power is getoggled")
                } else {
                    print("error api call")
                }
            }
            Task.resume()
            
            didChange.send(())
        }
    }
    
    
}
